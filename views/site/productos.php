<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\bootstrap\Progress;
use yii\bootstrap\Carousel;

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="productos">
    <div class="bg-image page-title">
        <div class="container-fluid">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="pull-right">
                <a href="#"><i class="fa fa-home fa-lg"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="#">Nuestros servicios</a>
            </div>
        </div>
    </div>
</div>

<div class="list-group " style="text-align: center">
    <a class="list-group-item list-group-item-action hgroup text-center wow fadeInUp" style="background-color: #262673; border-color: #262673; border-radius: 0;">
        <h1 class="list-group-item-heading" style="color: white; "><br>SOLO LOS MEJORES PRODUCTOS:<br><BR></h1>    
  </a>
</div>

<div class="row col-lg-offset-1" style="margin-top: 80px;">
    <div class="hgroup text-center wow fadeInUp col-lg-3 " data-wow-delay="0.3s">
        <div class="service-div" style="margin-top: 0px;">
                            <img class="faa-wrench animated-hover" src="img/refaccion2.png" style="width: 130px; margin-bottom: 8px;">
                            
                        </div>    
        <h1>Refacciones y componentes</h1>
            <h2>Nuevas y Reman<br>
                <h2>Para Motores a Diesel y Transmisiones</h2>
            <br>
        </div>
        <div class="hgroup text-center wow fadeInUp col-lg-3 col-lg-offset-1" data-wow-delay="0.3s">    
            <div class="service-div" style="margin-top: -7px;">
                            <img class="faa-wrench animated-hover" src="img/motor.png" style="width: 137px;">
                        </div>
            <h1>Motores Diesel Completos y <sup>3</sup>/<sub>4</sub></h1>
            <h2>Nuevos y Reman</h2>
        </div>
    <div class="hgroup text-center wow fadeInUp col-lg-3 col-lg-offset-1" data-wow-delay="0.3s">                
        <div class="service-div" style="margin-top: -20px;">
                            <img class="faa-wrench animated-hover" src="img/transmision1.png" style="width: 155px;">
                        </div>    
        <h1>Transmisiones Automáticas</h1>
            <h2>Nuevas y Retran</h2>
    </div>
</div>


<div class="list-group" style="text-align: center">
  <a class="list-group-item list-group-item-action  hgroup text-center wow fadeInUp mousear">
    <h1>PRODUCTOS</h1>    
  </a>
    <div class="container">
    <ul class="col-lg-3 list-unstyled"> 
        <li ><a style="color: black;" > <h4>Medias reparaciones</h4></a></li>
        <li ><a style="color: black;" > <h4>Kit de cilindros</h4></a></li>
        <li ><a style="color: black;" > <h4>Metales</h4></a></li>
        <li ><a style="color: black;" > <h4>Kits de metales</h4></a></li>   
    </ul>
            <ul class="col-lg-3 list-unstyled">        
                <li ><a style="color: black;" > <h4>Bombas de agua</h4></a></li>     
                <li ><a style="color: black;" > <h4>Bombas de aceite</h4></a></li>
                <li ><a style="color: black;" > <h4>Bombas de combustible</h4></a></li>
                <li ><a style="color: black;" > <h4>Inyectores</h4></a></li>
                <li ><a style="color: black;" > <h4>Jgos. de empaques</h4></a></li>        
    </ul>
        <ul class="col-lg-3 list-unstyled">        
            <li ><a style="color: black;" > <h4>Sensores</h4></a></li>
            <li ><a style="color: black;" > <h4>Arneses</h4></a></li>
            <li ><a style="color: black;" > <h4>Termostatos</h4></a></li>
            <li ><a style="color: black;" > <h4>Carter</h4></a></li>              
    </ul>
        <ul class="col-lg-3 list-unstyled">        
            <li ><a style="color: black;" > <h4>Retenes</h4></a></li>  
            <li ><a style="color: black;" > <h4>Válvulas check</h4></a></li>
            <li ><a style="color: black;" > <h4>Válvulas reguladoras</h4></a></li>
            <li ><a style="color: black;" > <h4>Mangas de inyector</h4></a></li>
            <li ><a style="color: black;" > <h4>Tensionador de bandas</h4></a></li>        
                    
    </ul>

    </div>
</div>

    <br><br><br>
    
    <div class="container text-center">
 <div class="col-lg-4 col-sm-4  wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic import-header text-center" style="background-size: 75%; ">
                    <img src="img/parte1.jpg" class="img-thumbnail"  style="width: 250px; border-radius: 40px; margin-top: 20px;"/>
                    <p>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</p>    
                </div>
 </div>
         <div class="col-lg-4 col-sm-4  wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic import-header text-center" style="background-size: 75%; ">
                    <img src="img/parte1.jpg" class="img-thumbnail"  style="width: 250px; border-radius: 40px; margin-top: 20px;"/>
                    <p>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</p>    
                </div>
 </div>
         <div class="col-lg-4 col-sm-4  wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic import-header text-center" style="background-size: 75%; ">
                    <img src="img/parte1.jpg" class="img-thumbnail"  style="width: 250px; border-radius: 40px; margin-top: 20px;"/>
                    <p>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</p>    
                </div>
 </div>
        <div class="col-lg-4 col-sm-4  wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic import-header text-center" style="background-size: 75%; ">
                    <img src="img/parte1.jpg" class="img-thumbnail"  style="width: 250px; border-radius: 40px; margin-top: 20px;"/>
                    <p>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</p>    
                </div>
 </div>
         <div class="col-lg-4 col-sm-4  wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic import-header text-center" style="background-size: 75%; ">
                    <img src="img/parte1.jpg" class="img-thumbnail"  style="width: 250px; border-radius: 40px; margin-top: 20px;"/>
                    <p>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</p>    
                </div>
 </div>
         <div class="col-lg-4 col-sm-4  wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic import-header text-center" style="background-size: 75%; ">
                    <img src="img/parte1.jpg" class="img-thumbnail"  style="width: 250px; border-radius: 40px; margin-top: 20px;"/>
                    <p>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</p>    
                </div>
 </div>
 </div>

<div class="big-hr color-1 wow zoomInUp" data-wow-delay="0.3s" style="border-bottom: 50px; margin-top: 100px;">
    <a href="" style="color: white;">
    <div class="wow" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight; margin-top: 15px;">
        <i class="fa fa-usd fa-3x" style="margin-right: 30px; margin-top: -30px; width: 50px; height: 50px;"></i>
    </div>
    <div class="text-center" style="">
        <h2>¡REALIZAMOS TU COTIZACIÓN COMPLETAMENTE GRÁTIS!</h2>
        <p>Aprovecha las ofertas que tenemos preparadas para ti.</p>
    </div>        
    </a>
</div>
    
<br><br>
<br><br>



