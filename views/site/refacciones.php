<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'REFACCIONES';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refacciones">
    <div class="bg-image page-title" style="margin-bottom: -80px;">
        <div class="container-fluid">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="pull-right">
                <a href="#"><i class="fa fa-home fa-lg"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="#">Nuestros servicios</a>
            </div>
        </div>
    </div>
</div>

<div class="">       

    <div class="container-fluid" style="margin-top: 100px; ">
        <div class="hgroup text-center wow fadeInUp" data-wow-delay="0.3s">
            <h1>SOLO LAS MEJORES REFACCIONES</h1>
            <h2>Por parte de profesionales</h2>
        </div>
        <ul class="nav nav-tabs wow zoomIn" data-wow-delay="0.3s" id="filter">                
            <li class="active"><a href="#" data-filter=".allservices">TODAS LAS PIEZAS</a></li>
            <li><a href="#" data-filter=".uno">TIPO 1</a></li>
            <li><a href="#" data-filter=".dos">TIPO 2</a></li>
            <li><a href="#" data-filter=".tres">TIPO 3</a></li>
            <li><a href="#" data-filter=".cuatro">TIPO 4</a></li>
            <li><a href="#" data-filter=".warehousing">TIPO 5</a></li>
            <li><a href="#" data-filter=".packaging">TIPO 6</a></li>
        </ul>
    </div>
</div>
<div class="container-fluid inner-offset wow zoomIn" data-wow-delay="0.3s">
    <div class="tab-content row services">
        <div class="tab-pane active isotope-filter">						
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices uno" style="">
                <img class="full-width" style="" src="img/objeto1.jpeg" alt="Img">
                <h4>TIPO 1</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. </p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>


            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices dos">
                <img class="full-width" src="img/objeto2.jpeg" alt="Img">
                <h4>TIPO 2</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna.</p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices dos">
                <img class="full-width" src="img/objeto2.jpeg" alt="Img">
                <h4>TIPO 2</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna.</p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices tres ">
                <img class="full-width" src="img/objeto3.jpeg" alt="Img">
                <h4>TIPO 3</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. </p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>

            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices cuatro">
                <img class="full-width" src="img/objeto4.jpeg" alt="Img">
                <h4>TIPO 4</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna.</p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices dos">
                <img class="full-width" src="img/objeto2.jpeg" alt="Img">
                <h4>TIPO 2</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna.</p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices tres ">
                <img class="full-width" src="img/objeto3.jpeg" alt="Img">
                <h4>TIPO 3</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. </p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>

            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices uno">
                <img class="full-width" src="img/objeto1.jpeg" alt="Img">
                <h4>TIPO 1</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna.</p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices cuatro">
                <img class="full-width" src="img/objeto4.jpeg" alt="Img">
                <h4>TIPO 4</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna.</p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices uno ">
                <img class="full-width" src="img/objeto1.jpeg" alt="Img">
                <h4>TIPO 1</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. </p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>						
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices dos">
                <img class="full-width" src="img/objeto2.jpeg" alt="Img">
                <h4>TIPO 2</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna.</p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices tres">
                <img class="full-width" src="img/objeto3.jpeg" alt="Img">
                <h4>TIPO 3</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna.</p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices cuatro">
                <img class="full-width" src="img/objeto4.jpeg" alt="Img">
                <h4>TIPO 4</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna.</p>
                <<a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices warehousing railway">
                <img class="full-width" src="media/3-column-info/8.jpg" alt="Img">
                <h4>SAFE & SECURE DELIVERY</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. </p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices seafreight airfreight railway">
                <img class="full-width" src="media/3-column-info/9.jpg" alt="Img">
                <h4>SAFE & SECURE DELIVERY</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. </p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices transportation">
                <img class="full-width" src="media/3-column-info/3.jpg" alt="Img">
                <h4>SAFE & SECURE DELIVERY</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. </p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices airfreight packaging warehousing">
                <img class="full-width" src="media/3-column-info/1.jpg" alt="Img">
                <h4>SAFE & SECURE DELIVERY</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. </p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices seafreight airfreight transportation">
                <img class="full-width" src="media/3-column-info/8.jpg" alt="Img">
                <h4>SAFE & SECURE DELIVERY</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. </p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
            <div class="service-item col-xs-12 col-sm-4 isotope-item allservices seafreight transportation packaging warehousing railway">
                <img class="full-width" src="media/3-column-info/2.jpg" alt="Img">
                <h4>SAFE & SECURE DELIVERY</h4>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. </p>
                <a class="btn btn-success btn-sm" href="#">VER PIEZA</a>
            </div>
        </div>
    </div>               
</div>

<div class="container-fluid block-content inner-offset">
    <div class="hgroup text-center wow fadeInUp" data-wow-delay="0.3s">
        <h1>SOLO LAS MEJORES REFACCIONES</h1>
        <h2>Por parte de profesionales</h2>
    </div>
    <div class="row main-grid team hover-eff">
        <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto1.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto2.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>
        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto3.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto4.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto5.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto6.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto1.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>
        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto2.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto3.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto4.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto5.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto6.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto1.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>
        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto2.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto3.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto4.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>

        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto5.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>						
        </div>
        <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="userpic" style="background-image:url(img/objeto6.jpeg);">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
            <h4>Tortnillo: 139jps</h4>
            <h4>Precio: $1513.40 <small>(Oferta) </small></h4>
            <br>					
        </div>


    </div>
</div> 

