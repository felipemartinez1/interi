<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'HISTORIA';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bg-image page-title">
        <div class="container-fluid">
           <h1><?= Html::encode($this->title) ?></h1>
            <div class="pull-right">
                <a href="#"><i class="fa fa-home fa-lg"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="06_services.html">Nuestros servicios</a>
            </div>
        </div>
</div>

<div class="container-fluid block-content">
				<div class="row">
					<div class="col-sm-6 wow fadeInLeft" data-wow-delay="0.3s">
						<h1>AGASA <br> </h1>
                                                <div class="text-justify" style="font-size: 125%">
                                                <p>Con experiencia de 18 años en el mercado a nivel Nacional que nos respaldan en conversión de GASOLINA a GAS.                                               Hemos cubierto el mercado principal de la ciudad y el estado.

<p>Gracias a nuestra labor y empeño nos hicimos acreedores a la distribución de la marca más prestigiada a nivel mundial BRC de
    origen Europeo (Italia), con presencia en más de 80 países en el mundo.Además contamos con SERVICIO A UNIDADES CON SISTEMA DE GAS, REFACCIONES BRC E IMPCO, DICTAMEN NOM-
005, TODO LO RELACIONADO A GAS AUTOMOTRIZ Y MONTACARGAS.</p>
                                          </div>
					</div>
					<div class="col-sm-6 wow fadeInRight" data-wow-delay="0.3s">
						<img class="full-width" src="img/principal4.jpeg" alt="Img">
						<p>Edificio donde inició AGASA</p>
					</div>
				</div>
			</div>


