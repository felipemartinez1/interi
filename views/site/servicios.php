<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\bootstrap\Progress;
use yii\bootstrap\Carousel;

$this->title = 'SERVICIOS';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="servicios">
    <div class="bg-image page-title">
        <div class="container-fluid">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="pull-right">
                <a href="#"><i class="fa fa-home fa-lg"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="#">Nuestros servicios</a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid block-content">
    <div class="col-lg-3">
        <div class="hgroup text-center wow fadeInUp" data-wow-delay="0.3s">
            <h1>DISTRIBUIDOR AUTORIZADO</h1>
            <img src="img/brc.gif" style="width:  150px;">
        </div>
    </div>
    <div class="col-lg-8">
        <div class="hgroup text-center wow fadeInUp" data-wow-delay="0.3s">
            <h1>CONVERSIONES</h1>
            <h2>¿QUÉ HACEMOS?</h2>
            <br>
        </div>
        <div class="hgroup text-center wow fadeInUp" data-wow-delay="0.3s">    
            <h1>TIPOS DE CONVERSIONES</h1>
            <h2>COSTOS</h2>
            <br><br><br>
            <h1>MANTENIMIENTO</h1>
            <h2>COSTOS</h2>
        </div>
        <div class="hgroup text-center wow fadeInUp" data-wow-delay="0.3s">    
            <h1>OFERTAS</h1>
            <h2>Descripción de la oferta</h2>
        </div>
        <br>
        <div class="hgroup text-center wow fadeInUp" data-wow-delay="0.3s">
            <h1>NUESTROS SERVICIOS</h1>
            <h2>SIEMPRE LO MEJOR</h2>
        </div>

        <div class="row our-services">
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">
                <a href="#">
                    <span><i class="fa fa-car"></i></span>
                    <h4>SERVICIO</h4>
                    <p>Descripción del servicio</p>
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">
                <a href="#">
                    <span><i class="fa fa-hand-o-left"></i></span>
                    <h4>SERVICIO</h4>
                    <p>Descripción del servicio</p>
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">
                <a href="#">
                    <span><i class="fa fa-file-movie-o"></i></span>
                    <h4>SERVICIO</h4>
                    <p>Descripción del servicio</p>
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">
                <a href="#">
                    <span><i class="fa fa-cloud"></i></span>
                    <h4>SERVICIO</h4>
                    <p>Descripción del servicio</p>
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">
                <a href="#">
                    <span><i class="fa fa-battery-full"></i></span>
                    <h4>SERVICIO</h4>
                    <p>Descripción del servicio</p>
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">
                <a href="#">
                    <span><i class="fa fa-arrow-circle-left"></i></span>
                    <h4>SERVICIO</h4>
                    <p>Descripción del servicio</p>
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">
                <a href="#">
                    <span><i class="fa fa-cart-arrow-down"></i></span>
                    <h4>SERVICIO</h4>
                    <p>Descripción del servicio</p>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid block-content percent-blocks" data-waypoint-scroll="true">
    <div class="row stats">
        <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="chart" data-percent="230">
                <span><i class="fa fa-folder-open"></i></span>
                <span class="percent">150</span>Proyectos realizados
            </div>
        </div>
        <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="chart" data-percent="68">
                <span><i class="fa fa-users"></i></span>
                <span class="percent">200</span>Equipo de trabajo
            </div>
        </div>
        <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="chart" data-percent="147">
                <span><i class="fa fa-truck"></i></span>
                <span class="percent">+300</span>Conversiones realizadas
            </div>
        </div>
        <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="chart" data-percent="105">
                <span><i class="fa fa-heart"></i></span>
                <span class="percent">+3,500</span>Clientes satisfechos
            </div>
        </div>
    </div>
</div>


<br><br>   

<div class="big-hr color-1 wow zoomInUp" data-wow-delay="0.3s" style="border-bottom: 50px;">
    <div class="wow" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight; margin-top: 15px;">
        <img src="img/dinero.png" style="margin-right: 30px; margin-top: -30px; width: 50px; height: 50px;"/>
    </div>
    <div class="text-center" style="">
        <h2>¡REALIZAMOS TU COTIZACIÓN COMPLETAMENTE GRÁTIS!</h2>
        <p>Aprovecha las ofertas que tenemos preparadas para ti.</p>
    </div>        
    <div><a class="btn btn-success btn-lg" style="border-top: 20px; margin-left: 40px;" href="index.php?r=site/contacto">COTIZAR</a></div>
</div>
<br><br>
<br><br>