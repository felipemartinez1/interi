<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\bootstrap\Progress;
use yii\bootstrap\Carousel;

$this->title = 'Empresa';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="empresa">
    <div class="bg-image page-title">
        <div class="container-fluid">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="pull-right">
                <a href="#"><i class="fa fa-home fa-lg"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="#">Nuestros servicios</a>
            </div>
        </div>
    </div>
</div>



<div class="container inner-offset">
    <div class=" info-texts wow fadeIn col-lg-offset-1" data-wow-delay="0.3s" style="border: solid; border-color: white; margin-bottom: 50px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 text-center" style="margin-left: 70px;">
                    <p style="color: white; font-size: 200%;">¿QUIÉNES<br><br style="margin-bottom: -4px;"> SOMOS?</p>
                </div>

                <div class=" col-sm-8 col-md-8 col-lg-8" style="background-color: white;">
                    <p style="font-size: 120%; text-align: center;">Somos una empresa Importadora y Comercializadora de Refacciones, Componentes, Motores a Diesel
                        y Transmisiones Automáticas, orientada a ofrecer un eficiente servicio de abastecimiento
                        en los sectores y mercados donde participamos activamente.</p>
                </div>
            </div>
        </div>
    </div>

    <div class=" info-texts wow fadeIn col-lg-offset-1" data-wow-delay="0.3s" style="border: solid; border-color: white; margin-bottom: 50px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 text-center" style="margin-left: 70px;">
                    <p style="color: white; font-size: 200%;">MISIÓN</p>
                </div>

                <div class=" col-sm-8 col-md-8 col-lg-8" style="background-color: white;">
                    <p style="font-size: 120%; text-align: center;">Ofrecer permanentemente Refacciones, Motores a Diesel y Transmisiones Automáticas
                        de Alta Calidad, Tecnología y Desempeño
                        a los sectores Automotriz, Marino e Industrial , apoyando con ello en forma eficiente
                        a nuestros clientes y usuarios, bajo una dinámica de actitud de servicio
                        así como una gestión correcta de negocio, en los mercados globales que participamos.</p>
                </div>
            </div>
        </div>
    </div>

    <div class=" info-texts wow fadeIn col-lg-offset-1" data-wow-delay="0.3s" style="border: solid; border-color: white; margin-bottom: 50px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 text-center" style="margin-left: 70px;">
                    <p style="color: white; font-size: 200%;">VISIÓN</p>
                </div>

                <div class=" col-sm-8 col-md-8 col-lg-8" style="background-color: white;">
                    <p style="font-size: 120%; text-align: center;">Continuar con la expansión constante, posicionamiento y consolidación de
                        Interiglobal – México, en la comercialización de sus productos
                        fortaleciendo los pilares de crecimiento y relación comercial,
                        alentando alcanzar la participación de cobertura deseable de la empresa
                        adoptando siempre, los parámetros ecológicos y de desarrollo tecnológico de la industria.</p>
                </div>
            </div>
        </div>
    </div>


</div>

<div class="row" style="margin-top: 0px;">
    <div class="text-center hdgroup wow fadeInUp" style="">
        <a class="list-group-item list-group-item-action  hgroup text-center wow fadeInUp">
            <h1 class="list-group-item-heading" style="font-size: 200%;">VALORES</h1>    
        </a>
    </div>


</div>
<div class="hgroup text-center wow fadeInUp" data-wow-delay="0.3s">    
    <div class="text-center" style="background-color: #262673; ">            
        <p style="font-size: 170%; margin-top: -15px;">
        <ul class="list-unstyled" style="color: white; font-size: 200%;">
            <br>
            <li>Integridad</li>
            <li>Respeto</li>
            <li>Compromiso</li>
            <li>Pasión</li>
            <li>Orientación al cliente</li>
            <br>
        </ul>               
        </p>
    </div>

</div>
<br>
<div class="hgroup text-center wow fadeInUp" data-wow-delay="0.3s">    
    <div class="row" style="margin-top: 40px; margin-bottom: -40px;">
        <div class="text-center hdgroup wow fadeInUp">
            <a class="list-group-item list-group-item-action  hgroup text-center wow fadeInUp">
                <h1 class="list-group-item-heading">SECTORES QUE ATENDEMOS</h1>    
            </a>
        </div>
    </div>
    <div class="container-fluid block-content percent-blocks" data-waypoint-scroll="true" style="align-content: center; ">
        <div class="row stats wow fadeInRight" style="margin-top: 0px; ">
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="chart" >
                    <span><i class="fa fa-square"></i></span>
                    <p style="font-size: 130%; margin-top: 20px; margin-left: -20px;">Automotriz</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="chart" data-percent="230">
                    <span><i class="fa fa-square"></i></span>
                    <p style="font-size: 130%; margin-top: 20px; margin-left: -20px;">Carga y peaje</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="chart" data-percent="230">
                    <span><i class="fa fa-square"></i></span>
                    <p style="font-size: 130%; margin-top: 20px; margin-left: -20px;">Industrial</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="chart" data-percent="230">
                    <span><i class="fa fa-square"></i></span>
                    <p style="font-size: 130%; margin-top: 20px; margin-left: -20px;">Marino</p>
                </div>
            </div>
        </div>
        <div class="row stats wow fadeInRight col-lg-offset-2" style="margin-top: -40px;">
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="chart" data-percent="230">
                    <span><i class="fa fa-square"></i></span>
                    <p style="font-size: 130%; margin-top: 20px; margin-left: -20px;">Generación</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="chart" data-percent="230">
                    <span><i class="fa fa-square"></i></span>
                    <p style="font-size: 130%; margin-top: 20px; margin-left: -20px;">Minero</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="chart" data-percent="230">
                    <span><i class="fa fa-square"></i></span>
                    <p style="font-size: 130%; margin-top: 20px; margin-left: -20px;">Petrolero</p>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<br>

