<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contacto';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-contact"> 
    <div class="contacto">
        <div class="bg-image page-title">
            <div class="container-fluid">
                <h1><?= Html::encode($this->title) ?></h1>
                <div class="pull-right">
                    <a href="#"><i class="fa fa-home fa-lg"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="#">Nuestros servicios</a>
                </div>
            </div>
        </div>
    </div>

    <iframe class="we-onmap wow fadeInUp" data-wow-delay="0.3s" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3171.4042427014083!2d-98.19896074938059!3d19.043656928936947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5b21261581c701c5!2sCatedral+de+Puebla!5e0!3m2!1ses!2smx!4v1469137837017"></iframe>

    <div class="container-fluid block-content">
        <div class="row main-grid">
            <div class="col-sm-4">
                <h4>OFICINAS</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                <div class="adress-details wow fadeInLeft" data-wow-delay="0.3s">
                    <div>
                        <span><i class="fa fa-location-arrow"></i></span>
                        <div><!--<strong>AGASA </strong><br>-->Calle Puebla No. 0000, Col. Puebla, Puebla. Pue.</div>
                    </div>
                    <div>
                        <span><i class="fa fa-phone"></i></span>
                        <div>(55) 5035 2805
                        </div>
                    </div>
                    <div>
                        <span><i class="fa fa-envelope"></i></span>
                        <div>Interiglobal@com.mx</div>
                    </div>
                    <div>
                        <span><i class="fa fa-clock-o"></i></span>
                        <div>Lun - Sab<br> 8.00 - 19.00</div>
                    </div>
                    <img src="img/contactoimg.jpeg" style="width: 300px; margin-top: 30px;">
                </div>
                <br><br><hr><br>                
            </div>


            <div class="col-sm-8 wow fadeInRight" data-wow-delay="0.3s">
                <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>                                          
                    <div class="alert alert-success">
                        Gracias por contactar con AGASA. Enseguida tendrás una respuesta.
                    </div>
                <?php else: ?>
                    <h4>Envíanos un mensaje</h4>
                    <p>Para cualquier duda o comentario
le agradecemos llenar el siguiente formulario,
será un gusto para nosotros atenderle</p>
                    <div id="success"></div>

                    <?php
                    $form = ActiveForm::begin([
                                'id' => 'contact-form',
                                'class' => 'reply-form form-inline',
                    ]);
                    ?>                               
                    <div class="row form-elem">
                        <div class="col-sm-6 form-elem"> 
                            <div class="default-inp form-elem" style="">
                                <i class="fa fa-user"></i>
                                <?= $form->field($model, 'name')->textInput([ 'placeholder' => 'Nombre', 'id' => 'user-name', 'class' => 'form-control','style'=>''])->label(false) ?>                                                            
                            </div>  
                        </div>
                        <div class="col-sm-6"> 
                            <div class="default-inp form-elem">
                                <i class="fa fa-user"></i>
                                <?= $form->field($model, 'apellido')->textInput(['placeholder' => 'Apellido', 'id' => 'user-lastname'])->label(FALSE) ?>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6"> 
                            <div class="default-inp">
                                <i class="fa fa-envelope"></i>
                                <?= $form->field($model, 'email')->input('email', ['placeholder' => 'Email'])->label(FALSE) ?>
                            </div>  
                        </div>

                        <div class="col-sm-6"> 
                            <div class="default-inp form-elem">
                                <i class="fa fa-phone"></i>
                                <?= $form->field($model, 'telefono')->input('integer', ['placeholder' => 'Telefono'])->label(FALSE) ?>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="col-sm-12">
                        <div class="row">                             
                            <div class="default-inp">                                    
                                <?= $form->field($model, 'subject')->input('text', ['placeholder' => 'Asunto'])->label(FALSE) ?>
                            </div>                              
                        </div>
                        <br>
                        <div class="row form-elem">                              
                            <div class="default-inp form-elem" style="margin-bottom: 20px;">                                    
                                <?= $form->field($model, 'body')->textArea(['rows' => 6, 'placeholder' => 'Comentario'])->label(FALSE) ?>
                            </div>                                               
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 80px;">                        
                        <div class="g-recaptcha" data-sitekey="6Le-MCUTAAAAAIwr9FyW-oKVGoeQRerjNlQ2f_6t"></div>
                    </div>
                    <?=
                    $form->field($model, 'reCaptcha')->widget(
                            \himiklab\yii2\recaptcha\ReCaptcha::className(), ['siteKey' => '6Le-MCUTAAAAAIwr9FyW-oKVGoeQRerjNlQ2f_6t']
                    )
                    ?>

                    <div class="form-elem">                        
                        <?= Html::submitButton('Enviar mensaje', ['class' => 'btn btn-success btn-default', 'name' => 'contact-button', '']) ?>
                    </div>           
                    <?php ActiveForm::end(); ?>
                <?php endif; ?> 
            </div> 
        </div>
    </div>    
</div>




