<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\bootstrap\Progress;
use yii\bootstrap\Carousel;

$this->title = 'Marcas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="marcas">
    <div class="bg-image page-title">
        <div class="container-fluid">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="pull-right">
                <a href="#"><i class="fa fa-home fa-lg"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="#">Nuestros servicios</a>
            </div>
        </div>
    </div>
</div>


<div class="list-group " style="text-align: center; border-color: #262673;">
    <a class="list-group-item list-group-item-action hgroup text-center wow fadeInUp" style="background-color: #262673; border-color: #262673; border-radius: 0;">
        <h1 class="list-group-item-heading" style="color: white; "><br>MANEJAMOS LAS MEJORES MARCAS:<br><BR></h1>    
  </a>
</div>
<div class="container-fluid block-content inner-offset" >
    <div class="row team hover-eff" style="margin-top: -70px;">        
        <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
            <div class="userpic" style="background-image:url(img/detroit.png); background-size: 67%;">
                <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                    Lorem ipsum dolor sit amet consectetur</span>
            </div>
        </div>
            <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic" style="background-image:url(img/marca-detroit.jpeg); background-size: 68%;">
                    <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</span>
                </div>						
            </div>
        <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s">
                <div class="userpic" style="background-image:url(img/mtu_logo.jpg); margin-bottom: 30px;  background-size: 70%;">
                    <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</span>
                </div>
            </div>
        <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic" style="background-image:url(img/mercedes.png);  background-size: 55%;">
                    <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</span>
                </div>
            </div>  
        <div class="col-lg-4 col-sm-4  wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic import-header text-center" style="background-size: 75%; ">
                    <img src="img/logo.png" class="img-thumbnail"  style="width: 250px; border-radius: 40px; margin-top: 20px;"/><h1 class="text-center" style="margin-top: 0px; color: #262673;">IGMX</h1>
                    <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</span>    
                </div>
            
            </div> 
        <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic" style="background-image:url(img/marca-john.jpeg);  background-size: 64%;">
                    <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</span>
                </div>
            </div>        
            
            <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px; ">
                <div class="userpic" style="background-image:url(img/cummins.png);  background-size: 50%;">
                    <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</span>
                </div>
            </div>
            <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px;">
                <div class="userpic" style="background-image:url(img/marca-allison.jpeg);   background-size: 66%;">
                    <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</span>
                </div>
            </div>
        <br><br>
         <div class="col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="margin-bottom: 30px; ">
                <div class="userpic" style="background-image:url(img/marca-cat.jpeg); background-size: 65%;">
                    <span>Integer congue elit non semper laore du lectus orc posuer nisl tempor lacus fel sed elite nisal mauris led
                        Lorem ipsum dolor sit amet consectetur</span>
                </div>
            </div>
        
            
        </div>
        </div>
<div class="big-hr color-1 wow zoomInUp" data-wow-delay="0.3s" style="margin-bottom: 50px; margin-top: -70px;">
    <a href="" style="color: white;">
    <div class="wow" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight; margin-top: 15px;">
        <i class="fa fa-usd fa-3x" style="margin-right: 30px; margin-top: -30px; width: 50px; height: 50px;"></i>
    </div>
    <div class="text-center" style="">
        <h2>¡REALIZAMOS TU COTIZACIÓN COMPLETAMENTE GRÁTIS!</h2>
        <p>Aprovecha las ofertas que tenemos preparadas para ti.</p>
    </div>        
    </a>
</div>
</div>
    
