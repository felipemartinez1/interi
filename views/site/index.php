<?php
$this->title = 'Interiglobal';
?>
<div class="site-index">
    <!--        SLIDER          -->
    <div width="100%" id="owl-main-slider" class="owl-carousel enable-owl-carousel" data-single-item="true" data-pagination="false" data-auto-play="true" data-main-slider="true" data-stop-on-hover="true">
        <div class="item">
            <img src="img/img3.jpeg" alt="slider">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">                    
                    <div style=" width:100px; margin-top: 60px;">
                        <a class="prev"><i class="fa fa-angle-left"></i></a>
                        <a class="next"><i class="fa fa-angle-right"></i></a>
                    </div> 
                    <div style="width: 500px; margin-top: 5%; text-align: center;" class="container-fluid col-lg-offset-11">
                        <h1 style="font-size: 200%; color: white;">INDUSTRIAL, MARINO, GENERACIÓN, PETROLERO Y MINERÍA.</h1>
                    </div>

                </div>
            </div>
        </div>
        <div class="item">
            <img src="img/img1.jpeg" alt="slider">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">
                    <div style=" width:100px; margin-top: 60px;">
                        <a class="prev"><i class="fa fa-angle-left"></i></a>
                        <a class="next"><i class="fa fa-angle-right"></i></a>
                    </div>                                  
                </div>
                <div style="width: 500px; margin-top: 33%; text-align: center;" class="container-fluid col-lg-offset-7">

                    <h1 style="font-size: 200%; color: white;">Refacciones y Componentes
                        Para Motores Diesel</h1>
                </div>
            </div>
        </div>

        <div class="item">
            <img src="img/img2.jpeg" alt="slider">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">
                    <div style=" width:100px; margin-top: 60px;">
                        <a class="prev"><i class="fa fa-angle-left"></i></a>
                        <a class="next"><i class="fa fa-angle-right"></i></a>
                    </div> 
                    <div style="display:table-cell;">
                        <h1 style="font-size: 200%;">INTERIGLOBAL - MÉXICO</h1>
                        <h3 class="text-center" style=" color: white; margin-top: 5px;">¡Un negocio diferente!</h3>
                    </div>

                </div>
            </div>
        </div>
    </div>







    <div class="row" style="background-color: #262673;">
        <div class="hgroup text-right wow fadeInUp content content-container col-lg-offset-2 col-lg-8" data-wow-delay="0.3s" style="margin-top: 90px;border-bottom: 400px; ">    
            <div class="hgroup text-center wow fadeInUp" data-wow-delay="0.3s">    
                <div class="text-center" style="margin-top: 15px; margin-bottom: 45px; color: white;">
                    <p style="font-size: 180%;">Rediseñando la comercialización de refacciones y motores
                        a diesel, con un modelo y gestión de negocio
                        correcto, practico, eficiente y confiable
                        con visión pro-activa a los mercados y marcas globales.</p>

                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid partners block-content" >
        <div class="hgroup title-space wow fadeInLeft" data-wow-delay="0.3s">
            <h1>NUESTROS CLIENTES</h1>
            <h2>SABEN EN QUIEN CONFIAR</h2>
        </div>
        <div id="partners" class="owl-carousel enable-owl-carousel" data-pagination="false" data-navigation="true" data-min450="2" data-min600="2" data-min768="4" style="background-color: white;">
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="img/marca1.jpeg" alt="Img" style="width: 170px; background-color: white;"></a></div>
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="img/marca2.jpeg" alt="Img" style="width: 170px;"></a></div>
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="img/coca.jpeg" alt="Img" style="width: 170px;"></a></div>
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="img/marca1.jpeg" alt="Img" style="width: 170px;"></a></div>
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="img/marca2.jpeg" alt="Img" style="width: 170px;"></a></div>
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="img/coca.jpeg" alt="Img" style="width: 170px;"></a></div>
        </div>
    </div>

    <div class="darken-block inner-offset">
        <div class="container-fluid">
            <div class="hgroup">
                <h1>OPINIONES DE NUESTROS CLIENTES</h1>
                <h2></h2>
            </div>
            <div id="testimonials2" class="owl-carousel enable-owl-carousel" data-pagination="false" data-navigation="true" data-auto-play="true" data-min450="1" data-min600="2" data-min768="2">
                <div>
                    <div class="testimonial-content">
                        <span><i class="fa fa-quote-left"></i></span>
                        <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal pellentesque bibendum.</p>
                    </div>
                    <div class="text-right testimonial-author">
                        <h4>JOHN DEO</h4>
                        <small>Managing Director</small>
                    </div>
                </div>
                <div>
                    <div class="testimonial-content">
                        <span><i class="fa fa-quote-left"></i></span>
                        <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal pellentesque bibendum.</p>
                    </div>
                    <div class="text-right testimonial-author">
                        <h4>JOHN DEO</h4>
                        <small>Managing Director</small>
                    </div>
                </div>
                <div>
                    <div class="testimonial-content">
                        <span><i class="fa fa-quote-left"></i></span>
                        <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal pellentesque bibendum.</p>
                    </div>
                    <div class="text-right testimonial-author">
                        <h4>JOHN DEO</h4>
                        <small>Managing Director</small>
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <br><br><br><br><br><br>
    <!--
        <div class="big-hr color-1 wow zoomInUp" data-wow-delay="0.3s" style="border-bottom: 50px;">
        <a href="" style="color: white;">
        <div class="wow" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight; margin-top: 15px;">
            <i class="fa fa-usd fa-3x" style="margin-right: 30px; margin-top: -30px; width: 50px; height: 50px;"></i>
        </div>
        <div class="text-center" style="">
            <h2>¡REALIZAMOS TU COTIZACIÓN COMPLETAMENTE GRÁTIS!</h2>
            <p>Aprovecha las ofertas que tenemos preparadas para ti.</p>
        </div>        
        </a>
    </div>-->
    <br><br><br><br><br><br>

</div>
