<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <meta charset="utf-8">
        <link href="css/master.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/owl.carousel.css" title="color1" media="all" />
        <link rel="stylesheet" type="text/css" href="css/owl.theme.css" title="color1" media="all" />
        <!--<link rel="stylesheet" type="text/css" href="css/owl.transitions.css" title="color1" media="all"/>-->
        <link href="css/font-awesome-animation.css" rel="stylesheet" />
    </head>
    <body data-scrolling-animations="true">
<?php $this->beginBody() ?>
        <div class="">
            <header id="this-is-top">
                <div class="container-fluid">
                    <div class="topmenu row"> 
                        <nav class="col-sm-offset-3 col-md-offset-4 col-lg-offset-4 col-sm-6 col-md-5 col-lg-5">
                        </nav>
                        <nav class="text-right col-sm-3 col-md-3 col-lg-3">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                        </nav>
                    </div>
                    <div class="row header">
                        <div class="col-sm-3 col-md-3 col-lg-3" style="">
                            <a href="index.php" id=""> <img src="img/logo.png" class="img-responsive radio-inline img-thumbnail" style="width: 300px; height: 150px; border-radius: 60px 20px 40px 20px; border-left: -10px;"/>
                            </a>                            
                        </div>
                        <div class="col-sm-offset-3 col-md-offset-1 col-lg-offset-1 col-sm-8 col-md-8 col-lg-8">
                            <div class="text-right header-padding">
                                <div class="h-block"><span>Llámanos</span>(55) 5035 2805</div>
                                <div class="h-block"><span>Correo</span>Interiglobal@com.mx</div>
                                <div class="h-block"><span>Horario laboral</span>Lun - Sab   |   8.00am - 19.00pm</div>
                                <a class="btn btn-success" href="#" style="margin-top: 10px;">Cotización gratuita</a>
                                <!--<a class="btn btn-success" href="#">GET A FREE QUOTE</a>-->
                            </div>
                            <a href="index.php"><h3 style="color: #262673; margin-top: 5px; margin-left: 10px;" class="col-lg-offset-0"><u>Interiglobal - México </u></h3></a>
                        </div>
                    </div>
                    <br>
                    <div id="main-menu-bg" ></div>  
                    <a id="menu-open" href="#"><i class="fa fa-bars"></i></a> 
                    
                    <?php
                    NavBar::begin([
                        'brandLabel' => '',
                        'brandUrl' => Yii::$app->homeUrl,
                        'options' => [
                            'class' => 'main-menu navbar-main-slide dropdown', 'style' => 'margin-bottom: -20px;'
                        ],
                    ]);
                    echo Nav::widget([
                        'options' => ['class' => 'nav navbar-nav navbar-main dropdown'],
                        'items' => [
                            ['label' => 'Inicio', 'url'=>['site/index']],
                            ['label' => 'Empresa', 'url'=>['site/empresa']],
                            ['label' => 'Productos', 'url'=>['site/productos']],
                            ['label' => 'Marcas/Modelos','url'=>['site/marcas']],
                            ['label' => 'Contacto', 'url'=>['site/contacto']],
                        ],
                    ]);
                    NavBar::end();
                    ?>                           
                    <a id="menu-close" href="#"><i class="fa fa-times"></i></a>
                </div>
            </header>       
<?= $content ?>                          
            <footer>
                <div class="color-part2"></div>
                <div class="color-part"></div>
                <div class="container-fluid">
                    <div class="row block-content">
                        <div class="col-sm-4 wow zoomIn" data-wow-delay="0.3s">
                            <a href="#" class="logo-footer"></a>
                            <p>Integer congue elit non semper laoreet sed lectu orc posuer nisl tempor sed felis ac mauris ellent esque ndu ca urna Integer vitae felis.</p>
                            <div class="footer-icons">
                                <a href="#"><i class="fa fa-facebook-square fa-2x"></i></a>
                                <a href="#"><i class="fa fa-google-plus-square fa-2x"></i></a>
                                <a href="#"><i class="fa fa-twitter-square fa-2x"></i></a>
                                <a href="#"><i class="fa fa-pinterest-square fa-2x"></i></a>
                                <a href="#"><i class="fa fa-vimeo-square fa-2x"></i></a>
                            </div>
                        </div>
                        <div class="col-sm-2 wow zoomIn" data-wow-delay="0.3s">
                            <h4>Nuestra ofertas</h4>
                            <nav>
                                <a href="#">Oferta #1</a>
                                <a href="#">Oferta #2</a>
                                <a href="#">Oferta #3</a>
                                <a href="#">Oferta #4</a>
                                <a href="#">Oferta #5</a>
                                <a href="#">Oferta #6</a>
                            </nav>
                        </div>
                        <div class="col-sm-2 wow zoomIn" data-wow-delay="0.3s">
                            <h4>MAIN LINKS</h4>
                            <nav>
                                <a href="index.php">Home</a>
                                <a href="index.php?r=site%2Fempresa">Empresa</a>
                                <a href="index.php?r=site%2Fproductos">Productos</a>
                                <a href="index.php?r=site%2Fmarcas">Marcas</a>
                                <a href="index.php?r=site%2Fcontacto">Contacto</a>
                            </nav>
                        </div>
                        <div class="col-sm-4 wow zoomIn" data-wow-delay="0.3s">
                            <h4>Información de contacto</h4>
                            Everyday is a new day for us and we work really hard to satisfy our customers everywhere.
                            <div class="contact-info">
                                <span><i class="fa fa-location-arrow"></i><strong>INTERIGLOBAL</strong><br>3608 NewHill Station Ave CA, Newyork 33102 </span>
                                <span><i class="fa fa-phone"></i>(55) 5035 2805</span>
                                <span><i class="fa fa-envelope"></i>Interiglobal@com.mx   |   interiglobal.com</span>
                                <span><i class="fa fa-clock-o"></i>LUN - SAB 8.00am - 19.00pm</span>
                            </div>
                        </div>
                    </div>
                    <div class="copy text-right"><a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a>Created by <a href="#" style="font-size: 110%;">BitEvolution </a> &copy; </div>
                </div>
            </footer>
        </div>
<?php $this->endBody() ?>
        
        <script src="js/owl.carousel.min.js"></script><!--SLIDER-->        
        <!--Theme-->
        <script src="js/jquery.smooth-scroll.js"></script><!--MENU DESPLEGABLE-->
        <script src="js/wow.min.js"></script><!--ANIMACIONES-->
        <script src="js/jquery.placeholder.min.js"></script><!--SLIDER INICIO-->
        <script src="js/theme.js"></script><!--SLIDER INICIO-->
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </body>
    
</html>
<?php $this->endPage() ?>

