<?php

namespace app\models;
use Yii;
use yii\base\Model;


/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $apellido;
    public $email;
    public $telefono;
    public $subject;
    public $body;
    public $reCaptcha;

    //public $verifyCode;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name','apellido', 'email', 'subject', 'body'], 'required', 'message'=> 'Campo requerido'],
            ['telefono', 'integer'],
            // email has to be a valid email address
            ['email', 'email', 'message'=>'Introduzca un correo electrónico válido'],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6Le-MCUTAAAAACBCF-o0nbcT_FjuOJXUGNZd3Ow1']
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            //'verifyCode' => 'Codigo de verificación',
            'name'=>'Nombre: ',
            'apellido'=> 'Apellido: ',
            'email'=>'Email: ',
            'telefono'=>'Telefono: ',
            'subject'=>'Asunto: ',
            'body'=>'Comentario: ',
            
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        $content= '<p><b> Email: </b>'.$this->email.'</p>';
        $content.= '<p><b> Teléfono: </b>'.$this->telefono.'</p>';
        $content.= '<p><b> Nombre: </b>'.$this->name.'</p>';
        $content.= '<p><b> Apellido: </b>'.$this->apellido.'</p>';
        $content.= '<p><b> Asunto: </b>'.$this->subject.'</p>';
        $content.= '<p><b> Contenido: </b><br>'.$this->body.'</p>';
        if ($this->validate()) {
            Yii::$app->mailer->compose('@app/mail/layouts/html', ['content'=>$content])                    
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }
}
