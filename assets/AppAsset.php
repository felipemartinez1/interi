<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css'
        
        //'themes/transcargo/assets/switcher/css/switcher.css',
        #'themes/transcargo/assets/switcher/css/color1.css',
        #'themes/transcargo/assets/switcher/css/color2.css',
        #'themes/transcargo/assets/switcher/css/color3.css',
        #'themes/transcargo/assets/switcher/css/color4.css',
        #'themes/transcargo/assets/switcher/css/color5.css',
        #'themes/transcargo/assets/switcher/css/color6.css',
    ];
    public $js = [ 
        
        /*
        'themes/transcargo/js/jquery-1.11.3.min.js',
        'themes/transcargo/js/jquery-ui.min.js',
        'themes/transcargo/js/bootstrap.min.js',
        'themes/transcargo/js/modernizr.custom.js',
        'themes/transcargo/assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js',
        'themes/transcargo/js/waypoints.min.js',
        'themes/transcargo/jjs/jquery.easypiechart.min.js',
        'themes/transcargo/assets/loader/js/classie.js',
        'themes/transcargo/aassets/loader/js/pathLoader.js',
        'themes/transcargo/assets/loader/js/main.js',
        'themes/transcargo/js/classie.js',
        'themes/transcargo/assets/switcher/js/switcher.js',
        'themes/transcargo/assets/owl-carousel/owl.carousel.min.js',
        'themes/transcargo/assets/isotope/jquery.isotope.min.js',
        'themes/transcargo/js/jquery.smooth-scroll.js',
        'themes/transcargo/js/wow.min.js',
        'themes/transcargo/js/jquery.placeholder.min.js',
        'themes/transcargo/js/smoothscroll.min.js',
        'themes/transcargo/js/theme.js',
         * */
         
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        '\rmrevin\yii\fontawesome\AssetBundle'
    ];
}
